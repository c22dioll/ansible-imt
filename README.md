# ansible - devops- imt

Provisionning d'une base de donnée reliée à une application Kubernetes

## How to run it
***note:*** 
Un problème de DNS persiste. 
Pour le contuorner afin que worker envoie les données et que result les récupère, il faut hardcoder l'IP de la VM sur laquelle est hébergée la base de donnée dans le code de result et de worker.
Je conseil de faire un CTRL + F sur l'IP puis replace all par la votre car il faut également changer l'ip dans le manifeste de kubernetes et pour le backup

- Modifier toute les IP dans le code par l'IP de votre VM
- Modifier le nom du projet GCP par le votre (notamment dans le docker-compose.yaml)
- Configurer l'accès distant à GCP (voir [projet Kubernetes](https://gitlab.imt-atlantique.fr/c22dioll/kubernetes-imt))
- Mettre les images sur GCP (penser à modifier l'ip de la VM)

```
cd docker-project
docker compose build
docker compose push
```

- Créer votre cluster
- Lancer les playbooks :
```
    cd tp-ansible-voting-app
    ansible-playbook deploy_postgres.yaml
    ansible-playbook backup-database.yaml
```
- Depuis la racine du projet :
```
kubectl create -f manifest_ansible.yaml
```
